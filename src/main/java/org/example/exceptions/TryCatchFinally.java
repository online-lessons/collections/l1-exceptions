package org.example.exceptions;

public class TryCatchFinally {
    public static void main(String[] args) {
        try {
//            throw new RuntimeException("AAA");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Finally");
        }

        System.out.println(m());
    }

    public static int m() {
        try {
//            System.out.println(5 / 0);
            return 1;
        }catch (Exception e) {
            return 2;
        }finally {
            return 3;
        }
    }
}
