package org.example.exceptions.creating;

import java.util.Scanner;

public class ErrorsDemo {
    public static void main(String[] args) {
        // Device Errors
        // Physical Errors
        // User Input errors
        // Code errors


        userInputError();

    }

    private static void userInputError() {
        // user input error
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        System.out.println(num);
    }
}