package org.example.exceptions.catching;

public class ExceptionsCatchingDemo {
    public static void main(String[] args) {

        try {
            m1();
        }catch (RuntimeException re) {
            System.out.println(re.getMessage());
        }

    }

    public static void m1() {
        throw new RuntimeException("This method throws exceptions");
    }
}
