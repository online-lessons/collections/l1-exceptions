package org.example.exceptions.catching;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.InputMismatchException;

public class TryCatch {
    public static void main(String[] args) {

        try {
            m1();
            m2();
        } catch (FileNotFoundException | UnknownHostException e) { // java 7
            System.out.println(e.getMessage());
        }
    }

    public static void m1() throws FileNotFoundException {
    }

    public static void m2() throws UnknownHostException {
    }
}
