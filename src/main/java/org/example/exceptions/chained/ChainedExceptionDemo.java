package org.example.exceptions.chained;

public class ChainedExceptionDemo {
    public static void main(String[] args) {

        // Since 1.4

        m1();

    }

    private static void m1() {
        try {
            m2();
        } catch (RuntimeException e) {
            RuntimeException exception = new RuntimeException("M1 E");
            exception.initCause(e);
            throw exception;
        }
    }

    private static void m2() {
        throw new RuntimeException("M2 E");
    }
}
