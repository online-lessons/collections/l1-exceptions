package org.example.exceptions;

import java.util.Scanner;

public class TryWithResource {
    public static void main(String[] args) {

        // since java 7


        try (Scanner scanner = new Scanner(System.in)) {
            String next = scanner.next();
            System.out.println(next);
        }
    }
}
